
package com.hungpham.rxandroidtutorial.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.hungpham.rxandroidtutorial.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observer;
import rx.subjects.PublishSubject;

public class ExampleActivity4 extends AppCompatActivity {

    @BindView(R.id.counter_display)
    TextView mCounterDisplay;
    @BindView(R.id.increment_button)
    Button mIncrementButton;
    private PublishSubject<Integer> mCounterEmitter;

    private int mCounter = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example_4);
        ButterKnife.bind(this);

        mCounterEmitter = PublishSubject.create();
        mCounterEmitter.subscribe(new Observer<Integer>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Integer integer) {
                mCounterDisplay.setText(String.valueOf(integer));
            }
        });
    }

    @OnClick(R.id.increment_button)
    void onIncrementButtonClick(){
        mCounter++;
        mCounterEmitter.onNext(mCounter);
    }
}
