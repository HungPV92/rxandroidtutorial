package com.hungpham.rxandroidtutorial.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hungpham.rxandroidtutorial.R;
import com.hungpham.rxandroidtutorial.model.ModelMain;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hung Pham on 11/10/2016.
 */

public class AdapterMain extends RecyclerView.Adapter<AdapterMain.ViewHolder> {

    private Context mContext;
    private ArrayList<ModelMain> datas;
    private OnItemClickListener clickListener;

    public AdapterMain(Context mContext, ArrayList<ModelMain> datas, OnItemClickListener clickListener) {
        this.mContext = mContext;
        this.datas = datas;
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(datas.get(position), clickListener);
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public interface OnItemClickListener{
        void onClick(ModelMain main);
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tvName) TextView tvName;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(final ModelMain model, final OnItemClickListener listener){
            tvName.setText(model.getName());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(model);
                }
            });
        }
    }
}
