package com.hungpham.rxandroidtutorial;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.hungpham.rxandroidtutorial.activity.ExampleActivity;
import com.hungpham.rxandroidtutorial.activity.ExampleActivity2;
import com.hungpham.rxandroidtutorial.activity.ExampleActivity3;
import com.hungpham.rxandroidtutorial.activity.ExampleActivity4;
import com.hungpham.rxandroidtutorial.activity.ExampleActivity5;
import com.hungpham.rxandroidtutorial.activity.ExampleActivity6;
import com.hungpham.rxandroidtutorial.activity.LoginActivity;
import com.hungpham.rxandroidtutorial.adapter.AdapterMain;
import com.hungpham.rxandroidtutorial.model.ModelMain;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    @BindView(R.id.rcvMain) RecyclerView rcvMain;

    private AdapterMain mAdapterMain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        rcvMain.setHasFixedSize(true);

        rcvMain.setLayoutManager(new GridLayoutManager(this, 2));

        mAdapterMain = new AdapterMain(this, getItems(), new AdapterMain.OnItemClickListener() {
            @Override
            public void onClick(ModelMain main) {
                startActivity(main.getId());
            }
        });
        rcvMain.setAdapter(mAdapterMain);
    }

    private ArrayList<ModelMain> getItems(){
        ArrayList<ModelMain> datas = new ArrayList<>();
        for (int i= 1;i<=7;i++){
            ModelMain model = new ModelMain(i,"Example "+i);
            datas.add(model);
        }
        return datas;
    }

    private void startActivity(int id){
        Intent intent = null;
        switch (id){
            case 1:intent = new Intent(this, ExampleActivity.class);break;
            case 2:intent = new Intent(this, ExampleActivity2.class);break;
            case 3:intent = new Intent(this, ExampleActivity3.class);break;
            case 4:intent = new Intent(this, ExampleActivity4.class);break;
            case 5:intent = new Intent(this, ExampleActivity5.class);break;
            case 6:intent = new Intent(this, ExampleActivity6.class);break;
            case 7:intent = new Intent(this, LoginActivity.class);break;
        }
        startActivity(intent);
    }
}
