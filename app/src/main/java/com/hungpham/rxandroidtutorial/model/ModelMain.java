package com.hungpham.rxandroidtutorial.model;

/**
 * Created by Hung Pham on 11/10/2016.
 */

public class ModelMain {

    private int id;
    private String name;

    public ModelMain(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
